﻿using System.Threading.Tasks;
using Tilde.EMW.Contracts.Models.Common.LanguageDirection;
using Tilde.EMW.Contracts.Models.Common.Translation;
using Tilde.EMW.Contracts.Models.Services.Configuration.Provider;

namespace Tilde.MTApiIntegrator.Interfaces.Services
{
    public interface ITranslationService
    {
        /// <summary>
        /// Translate text
        /// </summary>
        /// <param name="translationRequest"> Translation request </param>
        /// <param name="provider"> Translation provider </param>
        /// <returns></returns>
        Task<Translation> Translate(TranslationRequest translationRequest, Provider provider);

        /// <summary>
        /// Get language directions from provider
        /// </summary>
        /// <param name="provider"> Translation provider </param>
        /// <returns></returns>
        Task<LanguageDirectionsResponse> GetLanguageDirections(Provider provider);
    }
}
