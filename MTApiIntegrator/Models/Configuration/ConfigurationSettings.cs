﻿namespace Tilde.MTApiIntegrator.Models.Configuration
{
    public class ConfigurationSettings
    {
        /// <summary>
        /// Base URL of the service
        /// </summary>
        public string BaseUrl { get; set; }
    }
}
