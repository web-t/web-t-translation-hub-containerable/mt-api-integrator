﻿namespace Tilde.MTApiIntegrator.Models.Configuration.Services
{
    public class ConfigurationService
    {
        /// <summary>
        /// Configuration service URI
        /// </summary>
        public string Uri { get; init; }
    }
}
