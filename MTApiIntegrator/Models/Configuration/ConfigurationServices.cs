﻿namespace Tilde.MTApiIntegrator.Models.Configuration
{
    public record ConfigurationServices
    {
        public Services.ConfigurationService ConfigurationService { get; init; }
    }
}
