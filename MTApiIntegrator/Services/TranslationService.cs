﻿using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Tilde.EMW.Contracts.Exceptions.TranslationSystem;
using Tilde.EMW.Contracts.Models.Common.LanguageDirection;
using Tilde.EMW.Contracts.Models.Common.Translation;
using Tilde.EMW.Contracts.Models.Services.Configuration.Provider;
using Tilde.MTApiIntegrator.Exceptions.Provider;
using Tilde.MTApiIntegrator.Interfaces.Services;


namespace Tilde.MTApiIntegrator.Services
{
    public class TranslationService : ITranslationService
    {
        private readonly ILogger _logger;
        private readonly IHttpClientFactory _httpClientFactory;

        public TranslationService(
            ILogger<TranslationService> logger,
            IHttpClientFactory httpClientFactory
        )
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        /// <inheritdoc/>
        public async Task<Translation> Translate(TranslationRequest translationRequest, Provider provider)
        {
            var client = CreateClientWithAuthorizationHeaders(provider);

            var result = await client.PostAsJsonAsync($"{provider.Url}/translate/text", translationRequest);

            if (result.StatusCode == System.Net.HttpStatusCode.GatewayTimeout)
            {
                throw new TranslationTimeoutException();
            }
            else if (result.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                throw new LanguageDirectionNotFoundException(translationRequest.Domain, translationRequest.SourceLanguage, translationRequest.TargetLanguage);
            }

            result.EnsureSuccessStatusCode();

            var json = await result.Content.ReadAsStringAsync();

            var translations = JsonSerializer.Deserialize<Translation>(json);

            return translations;
        }

        /// <inheritdoc/>
        public async Task<LanguageDirectionsResponse> GetLanguageDirections(Provider provider)
        {
            var client = CreateClientWithAuthorizationHeaders(provider);

            var result = await client.GetAsync($"{provider.Url}/translate/language-directions");

            result.EnsureSuccessStatusCode();

            var json = await result.Content.ReadAsStringAsync();

            var languageDirections = JsonSerializer.Deserialize<LanguageDirectionsResponse>(json);

            foreach (var item in languageDirections.LanguageDirections)
            {
                item.TargetLanguage = item.TargetLanguage.ToLower();
                item.SourceLanguage = item.SourceLanguage.ToLower();
            }

            return languageDirections;
        }

        /// <summary>
        /// Create an HTTP client with the necessary headers for communication with the provider
        /// </summary>
        /// <param name="provider"> Provider model </param>
        /// <returns></returns>
        /// <exception cref="ProviderConfigurationNotFoundException"></exception>
        private HttpClient CreateClientWithAuthorizationHeaders(Provider provider)
        {
            var client = _httpClientFactory.CreateClient();

            if (provider.Authentication == null)
            {
                throw new ProviderConfigurationNotFoundException();
            }

            if (!string.IsNullOrEmpty(provider.Authentication.ApiKey))
            {
                client.DefaultRequestHeaders.Add("x-api-key", provider.Authentication.ApiKey);
            }
            else
            {
                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
                       .GetBytes(provider.Authentication.BasicAuthentication.Username + ":" + provider.Authentication.BasicAuthentication.Password));

                client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded);
            }

            return client;
        }
    }
}
