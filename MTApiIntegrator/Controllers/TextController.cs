﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using System.Threading.Tasks;
using Tilde.EMW.Contracts.Enums.Error;
using Tilde.EMW.Contracts.Exceptions.TranslationSystem;
using Tilde.EMW.Contracts.Models.Common.Errors;
using Tilde.EMW.Contracts.Models.Common.Translation;
using Tilde.MTApiIntegrator.Exceptions.Provider;
using Tilde.MTApiIntegrator.Interfaces.Services;

namespace Tilde.MTApiIntegrator.Controllers
{
    /// <summary>
    /// API for text translation
    /// </summary>
    [ApiController]
    [Route("translate/text")]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class TextController : BaseController
    {
        private readonly ILogger<TextController> _logger;
        private readonly ITranslationService _translationService;
        private readonly IConfigurationService _configurationService;


        public TextController(
            ILogger<TextController> logger,
            IMapper mapper,
            ITranslationService translationService,
            IConfigurationService configurationService
        )
        {
            _logger = logger;
            _translationService = translationService;
            _configurationService = configurationService;
        }

        /// <summary>
        /// Get text translation
        /// </summary>
        /// <param name="request"> Translation request </param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(Translation))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Missing or incorrect parameters", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "An unexpected error occured", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.GatewayTimeout, Description = "Request timed out", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Language direction is not found", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.TooManyRequests, Description = "Too many requests", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.RequestEntityTooLarge, Description = "Maximum text size limit reached for the request", Type = typeof(APIError))]
        public async Task<ActionResult<Translation>> GetTranslation(TranslationRequest request)
        {
            try
            {
                var provider = await _configurationService.GetProvider();

                var response = await _translationService.Translate(request, provider);

                return Ok(response);
            }
            catch (TranslationTimeoutException ex)
            {
                _logger.LogError("Translation timeout: {Error}", ex.Message);

                return FormatAPIError(HttpStatusCode.GatewayTimeout, ErrorSubCode.GatewayTranslationTimedOut);
            }
            catch (LanguageDirectionNotFoundException ex)
            {
                _logger.LogError(ex, "Language direction not found, please provide correct translation data - source language, target language, and domain");

                return FormatAPIError(HttpStatusCode.NotFound, ErrorSubCode.GatewayLanguageDirectionNotFound);
            }
            catch (ProviderConfigurationNotFoundException ex)
            {
                _logger.LogError(ex, "Provider configuration not found, please configure it");

                return FormatAPIError(HttpStatusCode.NotFound, ErrorSubCode.GatewayGeneric);
            }
        }
    }
}
