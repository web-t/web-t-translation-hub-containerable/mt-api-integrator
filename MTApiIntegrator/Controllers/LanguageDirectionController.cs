﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using System.Threading.Tasks;
using Tilde.EMW.Contracts.Enums.Error;
using Tilde.EMW.Contracts.Models.Common.LanguageDirection;
using Tilde.MTApiIntegrator.Exceptions.Provider;
using Tilde.MTApiIntegrator.Interfaces.Services;


namespace Tilde.MTApiIntegrator.Controllers
{
    [ApiController]
    [Route("translate/language-directions")]
    public class LanguageDirectionController : BaseController
    {
        private readonly ILogger _logger;
        private readonly ITranslationService _translationService;
        private readonly IConfigurationService _configurationService;

        public LanguageDirectionController(
            ILogger<LanguageDirectionController> logger,
            ITranslationService translationService,
             IConfigurationService configurationService
        )
        {
            _logger = logger;
            _translationService = translationService;
            _configurationService = configurationService;
        }

        /// <summary>
        /// Lists available languages and domains
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(LanguageDirectionsResponse), Description = "")]
        public async Task<ActionResult<LanguageDirectionsResponse>> List()
        {
            try
            {
                var provider = await _configurationService.GetProvider();

                LanguageDirectionsResponse directions = await _translationService.GetLanguageDirections(provider);

                return Ok(directions);
            }
            catch (ProviderConfigurationNotFoundException ex)
            {
                _logger.LogError(ex, "Provider configuration not found, please configure it");

                return FormatAPIError(HttpStatusCode.NotFound, ErrorSubCode.GatewayGeneric);
            }
        }
    }
}
