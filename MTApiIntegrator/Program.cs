using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Tilde.MTApiIntegrator.Models.Mappings;
using Tilde.MTApiIntegrator.Extensions;
using Tilde.MTApiIntegrator.Models.Configuration;
using Tilde.MTApiIntegrator.Services;
using Tilde.MTApiIntegrator.Interfaces.Services;

var builder = WebApplication.CreateBuilder(args);

var configurationServices = builder.Configuration.GetSection("Services").Get<ConfigurationServices>();

builder.Host
    .UseSerilog((ctx, config) =>
    {
        config
            .Enrich.FromLogContext()
            .WriteTo.Debug()
            .WriteTo.Console()
            .ReadFrom.Configuration(builder.Configuration);
    });


builder.Services.AddHttpClient();

builder.Services.AddMemoryCache();

builder.Services.AddCorsPolicies();

builder.Services.Configure<ConfigurationServices>(builder.Configuration.GetSection("Services"));

builder.Services.AddHealthChecks();

builder.Services.AddControllers();

var configuration = builder.Configuration.GetSection("Configuration").Get<ConfigurationSettings>();

builder.Services.AddDocumentation(configuration);

var mappingConfig = new MapperConfiguration(config =>
{
    config.AddProfile(new MappingProfile());
});
builder.Services.AddSingleton(mappingConfig.CreateMapper());

builder.Services.AddScoped<ITranslationService, TranslationService>();
builder.Services.AddScoped<IConfigurationService, ConfigurationService>();

builder.Services.AddHttpContextAccessor();

var app = builder.Build();

app.UseDocumentation(configuration);

app.UseRouting();

app.UseAuthorization();
#if DEBUG
app.UseCorsPolicies();
#endif
app.MapControllers();

// Startup probe / readyness probe
app.MapHealthChecks("/health/ready", new HealthCheckOptions()
{

});

// Liveness 
app.MapHealthChecks("/health/live", new HealthCheckOptions()
{

});

app.Run();