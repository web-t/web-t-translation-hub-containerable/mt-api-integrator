# MT Api Integrator

MT Api Integrator

# Monitor

## Healthcheck probes

https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/

Startup probe / Readiness probe:

`/health/ready`

Liveness probe:

`/health/live`


# Test

Using docker compose
```
docker-compose up --build
```

Open Swagger
```
http://localhost:5005/swagger/index.html
```