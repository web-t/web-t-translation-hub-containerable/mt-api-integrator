﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Tilde.EMW.Contracts.Models.Services.Configuration.Provider;
using Tilde.MTApiIntegrator.Interfaces.Services;
using Tilde.MTApiIntegrator.Models.Configuration;

namespace Tilde.MTApiIntegrator.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly ILogger _logger;
        private readonly ConfigurationServices _configurationServices;
        private readonly IHttpClientFactory _httpClientFactory;

        public ConfigurationService(
            ILogger<ConfigurationService> logger,
            IOptions<ConfigurationServices> configurationServices,
            IHttpClientFactory httpClientFactory
        )
        {
            _logger = logger;
            _configurationServices = configurationServices.Value;
            _httpClientFactory = httpClientFactory;
        }

        /// <inheritdoc/>
        public async Task<Provider> GetProvider()
        {
            var client = _httpClientFactory.CreateClient();

            var result = await client.GetAsync($"{_configurationServices.ConfigurationService.Uri}/provider");

            result.EnsureSuccessStatusCode();

            var json = await result.Content.ReadAsStringAsync();

            var options = new JsonSerializerOptions();
            options.Converters.Add(new JsonStringEnumConverter());

            var provider = JsonSerializer.Deserialize<Provider>(json, options);

            return provider;
        }
    }
}
