﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using Tilde.MTApiIntegrator.Models.Configuration;

namespace Tilde.MTApiIntegrator.Extensions
{
    public static class DocumentationExtensions
    {
        /// <summary>
        /// Add swagger documentation
        /// </summary>
        /// <param name="services"> Service collection </param>
        /// <param name="configuration"> Configuration settings </param>
        /// <returns></returns>
        public static IServiceCollection AddDocumentation(this IServiceCollection services, ConfigurationSettings configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = nameof(MTApiIntegrator), Version = "v1" });

                c.EnableAnnotations();

                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{nameof(Tilde)}.{nameof(MTApiIntegrator)}.xml"));

                c.AddServer(new OpenApiServer()
                {
                    Url = configuration.BaseUrl
                });
            });

            return services;
        }

        /// <summary>
        /// Use documentation
        /// </summary>
        /// <param name="app"> Application builder </param>
        /// <param name="configuration"> Configuration settings </param>
        /// <returns></returns>
        public static IApplicationBuilder UseDocumentation(this IApplicationBuilder app, ConfigurationSettings configuration)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint(configuration.BaseUrl + "/swagger/v1/swagger.json", $"{nameof(MTApiIntegrator)} v1"));

            return app;
        }
    }
}
