﻿using System;

namespace Tilde.MTApiIntegrator.Exceptions.Provider
{
    public class ProviderConfigurationNotFoundException : Exception
    {
        public ProviderConfigurationNotFoundException() :
            base($"Provider configuration not found")
        {

        }
    }
}
