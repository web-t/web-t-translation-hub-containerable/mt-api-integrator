﻿using System.Threading.Tasks;
using Tilde.EMW.Contracts.Models.Services.Configuration.Provider;

namespace Tilde.MTApiIntegrator.Interfaces.Services
{
    public interface IConfigurationService
    {
        /// <summary>
        /// Get provider information
        /// </summary>
        /// <returns></returns>
        Task<Provider> GetProvider();
    }
}
